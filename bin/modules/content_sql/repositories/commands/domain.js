const Query = require('../queries/query');
const Command = require('./command');
const Model = require('./command_model');
const wrapper = require('../../../../helpers/utils/wrapper');
const logger = require('../../../../helpers/utils/logger');
const { InternalServerError, ConflictError } = require('../../../../helpers/error');
const { randomUUID } = require('crypto');

class Content {
  constructor(db) {
    this.command = new Command(db);
    this.query = new Query(db);
  }

  async postContent(payload) {
    const cfx = 'domain-postContent';
    const { title, uploader, rating, totalUser, details, material } = payload;
    const content = await this.query.duplicateContent({ title, uploader, rating, totalUser, details, material });
    if (content.data != '') {
      logger.log[(cfx, 'content already exist', 'error')];
      return wrapper.error(new ConflictError('content already exist'));
    }

    const contentData = Model.ContentData();
    contentData.id = randomUUID();
    contentData.title = title;
    contentData.uploader = uploader;
    contentData.rating = rating;
    contentData.totalUser = totalUser;
    contentData.details = details;
    contentData.material = material;

    const { data: result } = await this.command.insertContent(contentData);

    if (result.err) {
      logger.log[(cfx, 'content failed to insert', 'error')];
      return wrapper.error(new InternalServerError('content failed to insert'));
    }
    return wrapper.data(result.data);
  }

  async putContent(payload) {
    const cfx = 'domain-putContent';
    const { data: result } = await this.command.putContent(payload);
    if (result.err) {
      logger.log[(cfx, 'content failed to update', 'error')];
      return wrapper.error(new InternalServerError('content failed to update'));
    }
    return wrapper.data(result.data);
  }

  async deleteContent(id) {
    const cfx = 'domain-deleteContent';
    const content = await this.command.deleteContent(id);
    if (content.err) {
      logger.log[(cfx, 'content failed to delete', 'error')];
      return wrapper.error(new InternalServerError('content failed to delete'));
    }
    return wrapper.data(content.data);
  }
}

module.exports = Content;
