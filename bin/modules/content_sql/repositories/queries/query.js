class Query {
  constructor(db) {
    this.db = db;
  }

  async findContent(parameter) {
    const recordset = await this.db.prepareQuery('SELECT title, uploader, rating, total_user, details, material FROM content WHERE id = ?', parameter);
    return recordset;
  }

  async duplicateContent(parameter) {
    const { title } = parameter;
    const recordset = await this.db.prepareQuery(
      'SELECT title, uploader, rating, total_user, details, material FROM content WHERE title = ? ',
      [title]
    );
    return recordset;
  }

  async pageContent(parameter) {
    const { limit, offset } = parameter;
    const recordset = await this.db.prepareQuery('SELECT title, uploader, rating, total_user, details, material FROM content ORDER BY title ASC LIMIT ? OFFSET ?', [limit, offset]);
    return recordset;
  }

  async searchContent(parameter) {
    const { keyword } = parameter;
    const title = `%${keyword}%`;
    const uploader = `%${keyword}%`;
    const rating = `${parseInt(keyword)}%`;
    const total_user = `${parseInt(keyword)}%`;
    const details = `%${keyword}%`;
    const material = `%${keyword}%`;
    const recordset = await this.db.prepareQuery('SELECT title, uploader, rating, total_user, details, material FROM content WHERE title LIKE ? OR uploader LIKE ? OR rating LIKE ? OR total_user LIKE ? OR details LIKE ? OR material LIKE ?',
      [title, uploader, rating, total_user, details, material]
    );
    return recordset;
  }

  async indexContent(parameter) {
    const { keyword } = parameter;
    const title = `${keyword}`;
    const uploader = `${keyword}`;
    const rating = `${parseInt(keyword)}`;
    const total_user = `${parseInt(keyword)}`;
    const details = `${keyword}`;
    const material = `${keyword}`;
    const recordset = await this.db.prepareQuery('SELECT title, uploader, rating, total_user, details, material FROM content WHERE title = ? OR uploader = ? OR rating = ? OR total_user = ? OR details = ? OR material = ?',
      [title, uploader, rating, total_user, details, material]
    );
    return recordset;
  }
}

module.exports = Query;
