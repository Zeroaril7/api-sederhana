
const Content = require('./domain');
const Mongo = require('../../../../helpers/databases/mongodb/db');
const config = require('../../../../infra/configs/global_config');
const db = new Mongo(config.get('/mongoDbUrl'));

const postContent = async (payload) => {
  const content = new Content(db);
  const postCommand = async payload => content.postContent(payload);
  return postCommand(payload);
};

const putContent = async (payload, id) => {
  const content = new Content(db);
  const putCommand = async (payload, id) => content.putContent(payload, id);
  return putCommand(payload, id);
};

const deleteContent = async (id) => {
  const content = new Content(db);
  const deleteCommand = async (id) => content.deleteContent(id);
  return deleteCommand(id);
};


module.exports = {
  postContent,
  putContent,
  deleteContent
};
