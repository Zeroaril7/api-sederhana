const joi = require('joi');
const uuid = require('uuid');

const content = joi.object({
  title: joi.string().required(),
  uploader: joi.string().required(),
  rating: joi.number().required(),
  totalUser: joi.number().required(),
  details: joi.string().required(),
  material: joi.string().required(),
});

const ContentData = () => {
  return {
    id: uuid(),
    title: '',
    uploader: '',
    rating: 0,
    totalUser: 0,
    details: '',
    material: '',
  };
};


module.exports = {
  content,
  ContentData
};
