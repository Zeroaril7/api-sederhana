const ObjectId = require('mongodb').ObjectId;

class Command {

  constructor(db) {
    this.db = db;
  }

  async insertOneContent(document){
    this.db.setCollection('content');
    const result = await this.db.insertOne(document);
    return result;
  }

  async updateOneContent(payload, id) {
    this.db.setCollection('content');
    const result = await this.db.upsertOne({ _id: ObjectId(id) }, { $set: payload });
    return result;
  }

  async deleteOneContent(id) {
    this.db.setCollection('content');
    const parameter = {
      _id: ObjectId(id),
    };
    const result = await this.db.deleteOne(parameter);
    return result;
  }

}

module.exports = Command;
