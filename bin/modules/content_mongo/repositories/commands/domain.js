
const Query = require('../queries/query');
const Command = require('./command');
const Model = require('./command_model');
const wrapper = require('../../../../helpers/utils/wrapper');
const jwtAuth = require('../../../../auth/jwt_auth_helper');
const commonUtil = require('../../../../helpers/utils/common');
const logger = require('../../../../helpers/utils/logger');
const { NotFoundError, UnauthorizedError, ConflictError, InternalServerError } = require('../../../../helpers/error');
const { randomUUID } = require('crypto');

class Content {
  constructor(db){
    this.command = new Command(db);
    this.query = new Query(db);
  }

  async postContent(payload) {
    const cfx = 'domain-postContent';
    const { title, uploader, rating, totalUser, details, material } = payload;
    const content = await this.query.findOneContent({ title, uploader, rating, totalUser, details, material });
    if (content.data !== null) {
      logger.log[(cfx, 'content already exist', 'error')];
      return wrapper.error(new ConflictError('content already exist'));
    }

    const contentData = Model.ContentData();
    contentData.id = randomUUID();
    contentData.title = title;
    contentData.uploader = uploader;
    contentData.rating = rating;
    contentData.totalUser = totalUser;
    contentData.details = details;
    contentData.material = material;

    const { data: result } = await this.command.insertOneContent(contentData);

    if (result.err) {
      logger.log[(cfx, 'content failed to insert', 'error')];
      return wrapper.error(new InternalServerError('content failed to insert'));
    }
    return wrapper.data(result.data);
  }

  async putContent(payload, id) {
    const cfx = 'domain-putContent';
    const { data: result } = await this.command.updateOneContent(payload, id);

    if (result.err) {
      logger.log[(cfx, 'content failed to insert', 'error')];
      return wrapper.error(new InternalServerError('content failed to insert'));
    }
    return wrapper.data(result.data);
  }

  async deleteContent(id) {
    const cfx = 'domain-deleteContent';
    const content = await this.command.deleteOneContent(id);
    if (content.err) {
      logger.log[(cfx, 'content failed to delete', 'error')];
      return wrapper.error(new InternalServerError('content failed to delete'));
    }
    return wrapper.data(content.data);
  }
}

module.exports = Content;
