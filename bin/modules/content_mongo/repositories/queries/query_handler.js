
const Content = require('./domain');
const Mongo = require('../../../../helpers/databases/mongodb/db');
const config = require('../../../../infra/configs/global_config');
const db = new Mongo(config.get('/mongoDbUrl'));
const content = new Content(db);

const getContent = async (id) => {
  const getData = async () => {
    const result = await content.viewContent(id);
    return result;
  };
  const result = await getData();
  return result;
};

const getPageContent= async (parameter) => {
  const getPageContent = async () => {
    const result = await content.viewPageContent(parameter);
    return result;
  };
  const result = await getPageContent();
  return result;
};
module.exports = {
  getContent,
  getPageContent
};
