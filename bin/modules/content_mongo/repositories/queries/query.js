
const ObjectId = require('mongodb').ObjectId;

class Query {

  constructor(db) {
    this.db = db;
  }

  async findOneContent(parameter) {
    this.db.setCollection('content');
    const recordset = await this.db.findOne(parameter);
    return recordset;
  }

  async findById(id) {
    this.db.setCollection('content');
    const parameter = {
      _id: ObjectId(id)
    };
    const recordset = await this.db.findOne(parameter);
    return recordset;
  }

  async findPageContent(parameter) {
    const { name, limit, skip } = parameter;
    this.db.setCollection('content');
    const recordset = await this.db.findAllData(name, limit, skip);
    return recordset ;
  }
}

module.exports = Query;
