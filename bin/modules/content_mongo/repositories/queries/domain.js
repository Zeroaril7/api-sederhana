
const Query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const { NotFoundError } = require('../../../../helpers/error');

class Content {

  constructor(db){
    this.query = new Query(db);
  }

  async viewContent(id) {
    const content = await this.query.findById(id);
    if (content.err) {
      return wrapper.error(new NotFoundError('Can not find content'));
    }
    const { data } = content;
    return wrapper.data(data);
  }

  async viewPageContent(parameter) {
    const limit = 2;
    const { page } = parameter;
    const skip = limit * page;
    const fieldName = 'title';
    const pagination = { fieldName, limit , skip};
    const dataContent = await this.query.findPageContent(pagination);
    if (dataContent.err) {
      return wrapper.error(new NotFoundError('Can not find data content'));
    }

    const { data } = dataContent;
    return wrapper.data(data);
  }

}

module.exports = Content;
