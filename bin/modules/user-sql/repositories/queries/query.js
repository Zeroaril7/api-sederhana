
class Query {

  constructor(db) {
    this.db = db;
  }

  async findOneUser(parameter) {
    const { username } = parameter;
    const recordset = await this.db.prepareQuery('SELECT username, password FROM user WHERE username = ?', [username]);
    return recordset;
  }

  async findUser(parameter) {
    const { username } = parameter;
    const recordset = await this.db.prepareQuery('SELECT id, username, password FROM user WHERE username = ?', [username]);
    return recordset;
  }

  async findById(userId) {
    const recordset = await this.db.prepareQuery('SELECT username, password FROM user WHERE id = ?', [userId]);
    return recordset;
  }

}

module.exports = Query;
