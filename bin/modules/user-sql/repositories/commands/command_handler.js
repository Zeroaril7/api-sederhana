
const User = require('./domain');
const MySql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');
const db = new MySql(config.get('/mysqlConfig'));

const postDataLogin = async (payload) => {
  const user = new User(db);
  const postCommand = async payload => user.generateCredential(payload);
  return postCommand(payload);
};

const registerUser = async (payload) => {
  const user = new User(db);
  const postCommand = async payload => user.register(payload);
  return postCommand(payload);
};

const deleteUser = async (userId) => {
  const user = new User(db);
  const deleteCommand = async (userId) => user.deleteUser(userId);
  return deleteCommand(userId);
};

module.exports = {
  postDataLogin,
  registerUser,
  deleteUser
};
