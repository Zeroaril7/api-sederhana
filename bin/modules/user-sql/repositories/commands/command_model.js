const joi = require('joi');
const uuid = require('uuid');

const login = joi.object({
  username: joi.string().required(),
  password: joi.string().required()
});

const Register = () => {
  return {
    id: uuid(),
    username: '',
    password: ''
  };
};

module.exports = {
  login,
  Register
};
