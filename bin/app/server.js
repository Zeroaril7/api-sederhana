const restify = require('restify');
const corsMiddleware = require('restify-cors-middleware');
const project = require('../../package.json');
const basicAuth = require('../auth/basic_auth_helper');
const jwtAuth = require('../auth/jwt_auth_helper');
const wrapper = require('../helpers/utils/wrapper');
const userMongoHandler = require('../modules/user-mongo/handlers/api_handler');
const userSqlHandler = require('../modules/user-sql/handlers/api_handler');
const contentHandler = require('../modules/content_sql/handlers/api_handler');
const contentMongoHandler = require('../modules/content_mongo/handlers/api_handler');
const mySql = require('../helpers/databases/mysql/connection');
const mongo = require('../helpers/databases/mongodb/connection');

function AppServer() {
  this.server = restify.createServer({
    name: `${project.name}-server`,
    version: project.version,
  });

  this.server.serverKey = '';
  this.server.use(restify.plugins.acceptParser(this.server.acceptable));
  this.server.use(restify.plugins.queryParser());
  this.server.use(restify.plugins.bodyParser());
  this.server.use(restify.plugins.authorizationParser());

  // required for CORS configuration
  const corsConfig = corsMiddleware({
    preflightMaxAge: 5,
    origins: ['*'],
    // ['*'] -> to expose all header, any type header will be allow to access
    // X-Requested-With,content-type,GET, POST, PUT, PATCH, DELETE, OPTIONS -> header type
    allowHeaders: ['Authorization'],
    exposeHeaders: ['Authorization'],
  });
  this.server.pre(corsConfig.preflight);
  this.server.use(corsConfig.actual);

  // // required for basic auth
  this.server.use(basicAuth.init());

  // anonymous can access the end point, place code bellow
  this.server.get('/', (req, res) => {
    wrapper.response(res, 'success', wrapper.data('Index'), 'This service is running properly');
  });

  // User Mongo
  this.server.post('/api/users/mongo/v1', basicAuth.isAuthenticated, userMongoHandler.postDataLogin);
  this.server.get('/api/users/mongo/v1/:userId', jwtAuth.verifyTokenMongo, userMongoHandler.getUser);
  this.server.post('/api/users/mongo/v1/register', basicAuth.isAuthenticated, userMongoHandler.registerUser);
  this.server.del('/api/users/mongo/v1/:userId', jwtAuth.verifyTokenMongo, userMongoHandler.deleteUser);

  // User Sql
  this.server.post('/api/users/sql/v1', basicAuth.isAuthenticated, userSqlHandler.postDataLogin);
  this.server.get('/api/users/sql/v1/:userId', jwtAuth.verifyTokenSql, userSqlHandler.getUser);
  this.server.post('/api/users/sql/v1/register', basicAuth.isAuthenticated, userSqlHandler.registerUser);
  this.server.del('/api/users/sql/v1/:userId', jwtAuth.verifyTokenSql, userSqlHandler.deleteUser);

  // content sql
  this.server.post('/api/contents/sql/v1', jwtAuth.verifyTokenSql, contentHandler.postContent);
  this.server.get('/api/contents/sql/v1/:id',jwtAuth.verifyTokenSql,contentHandler.getContent);
  this.server.put('/api/contents/sql/v1', jwtAuth.verifyTokenSql,contentHandler.putContent);
  this.server.del('/api/contents/sql/v1/:id', jwtAuth.verifyTokenSql,contentHandler.deleteContent);
  this.server.get('/api/contents/sql/v1/page/:page',jwtAuth.verifyTokenSql, contentHandler.getPageContent);
  this.server.get('/api/contents/sql/v1/search/:keyword', basicAuth.isAuthenticated, contentHandler.getSearchContent);
  this.server.get('/api/contents/sql/v1/index/:keyword', basicAuth.isAuthenticated, contentHandler.getIndexContent);

  // content mongo
  this.server.post('/api/contents/mongo/v1', jwtAuth.verifyTokenMongo, contentMongoHandler.postContent);
  this.server.get('/api/contents/mongo/v1/:id', basicAuth.isAuthenticated, contentMongoHandler.getContent);
  this.server.put('/api/contents/mongo/v1/:id', jwtAuth.verifyTokenMongo, contentMongoHandler.putContent);
  this.server.del('/api/contents/mongo/v1/:id', jwtAuth.verifyTokenMongo, contentMongoHandler.deleteContent);
  this.server.get('/api/contents/mongo/v1/page/:page', jwtAuth.verifyTokenMongo, contentMongoHandler.getPageContent);

  //Initiation
  mySql.createConnectionPool;
  mongo.init();
}

module.exports = AppServer;
